// Tugas 9 
console.log(`           >>>>> Tugas9 <<<<<`         )
console.log('------ 1. Mengubah Fungsi Arrow ------')

// 1. function
/*  Soal

const golden = function goldenFunction () {
    console.log('this is golden !')
}
golden(); 
*/


// Jawaban 
golden = () => {console.log('this is Golden!')}
    golden();





// 2. Object Literal es6
console.log('------ 2. Object Literal ES6 ------')

const newFunction = (firstName, lastName) => {
    return {
       firstName,
        lastName,
        fullName: () => console.log(`${firstName} ${lastName}`) 
    }
}
newFunction(`William`, `Imoh`).fullName()




// .......... 3 .........
console.log('------ 3. Destructuring ------')

const newObject = {
    firstName : `Harry`,
    lastName : `Potter Holt`,
    destination : `Hogwarts React Conf`,
    occupation : `Vimulus Renderus!!!`
}

// const firstName = newObject.firstName;
// const lastName = newObject.lastName;
// const destination = newObject.destination;
// const occupation = newObject.occupation;

const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation)


console.log('------ 4. Array Spreading ------')

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east)
const combined = [west, east]
console.log(combined)




// ...... 5 .......
console.log('------ 5. Template Literal ------')

const planet = "Earth"
const view = "Glass"
var before = `lorem ${view} dolor sit amet, consesctetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.`

console.log(before)