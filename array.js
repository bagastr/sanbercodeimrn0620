// soal no.1 (Range)
var soal1 = "Soal no.1 Range"
console.log(soal1)



function range (startNum, finishNum) {
    var numbers = []
    if (startNum == null || finishNum == null) {
        return -1
    } for (startNum; startNum <= finishNum; startNum++) {
        numbers.push(startNum)
        return numbers
    } for (startNum; startNum >= finishNum; startNum--) {
        numbers.push(startNum)
        return numbers
    } 
    
} 
    

console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())


console.log('-------------------------------')


// Soal 2 Range withStep
var soal2 = "Soal no.2 Range"
console.log(soal2)

function rangeWithStep (startNum, finishNum, step) {
    var numbers = []

    if (startNum == null || finishNum == null || step == null) {
        return -1
    } for (startNum; startNum <= finishNum; startNum+= step) {
        numbers.push(startNum)
        return numbers
    } for (startNum; startNum >= finishNum; startNum-= step) {
        numbers.push(startNum)
        return numbers
    } for (startNum; startNum >= finishNum; startNum+= step) {
        numbers.sort((a, b) => a-b)
        return numbers
    } 
    
} 


console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))


console.log('-------------------------------')

// Soal 3 Sum of Range
var soal3 = "Soal no.3 Sum of Range"
console.log(soal3)


function sum(startNum, finishNum,step) {
    var number3 =[]
    var jumlah =0
    if(startNum == null || finishNum == null){
        step = 1
        return 1
    }
    if(step==null){
        step = 1
    }
    if(startNum < finishNum){
        for(startNum; startNum<=finishNum; startNum+=step)
        {
           
            number3.push(startNum)
            jumlah = jumlah + startNum
        }
        return jumlah
    }
    else{
        for(startNum; startNum>=finishNum; startNum-=step)
        {
            number3.push(startNum)
            jumlah = jumlah + startNum
        }
            return jumlah 
        
    }
}

console.log(sum(  1  , 10  ))
console.log(sum(  5  ,  50  , 2 ))
console.log(sum(  15 ,  10 ))
console.log(sum(20 ,10 , 2 ))
console.log(sum(1))
console.log(sum())

// Soal 4 Array Multidimensi
var soal4 = "Soal no.4 Multidimensi"
console.log(soal4)

 

var input = [ 
    ["0001", "Roman Alamsyah", "Bandar Lampung","21/05/1989", "membaca"], 
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "bermain gitar"], 
    ["0003", "Winona", "Ambon", "25/12/1965", "memasak"],
    ["0004", "Bintang Senjaya", "martapura", "6/4/1970", "berkebun"]
];

console.log("nomor ID: " + input [0][0])
console.log("Nama Lengkap: " + input [0][1])
console.log("Ttl: " + input [0][2] + " " + input [0][3])
console.log("Hobi: " + input [0][4])
console.log("\n")
console.log("nomor ID: " + input [1][0])
console.log("Nama Lengkap: " + input [1][1])
console.log("Ttl: " + input [1][2] + " " + input [1][3])
console.log("Hobi: " + input [1][4])
console.log("\n")
console.log("nomor ID: " + input [2][0])
console.log("Nama Lengkap: " + input [2][1])
console.log("Ttl: " + input [2][2] + " " + input [2][3])
console.log("Hobi: " + input [2][4])
console.log("\n")
console.log("nomor ID: " + input [3][3])
console.log("Nama Lengkap: " + input [3][1])
console.log("Ttl: " + input [3][2] + " " + input [3][3])
console.log("Hobi: " + input [3][4])
console.log("\n")





console.log('-------------------------------')

// Soal 5 
var soal5 = "Soal no.5 Balik Kata"
console.log(soal5)

function balikKata () {
    
}